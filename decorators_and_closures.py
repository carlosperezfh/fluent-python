# Decorators

# A KEy feature of decorators is that they run right after the decorated function is defined. That is usually at import time
# Consider registration.py

# Closures
# Closures only matters when you have nested fns
# A closure is function that retains the bindings of the free variables that exist
#$ when the function is defined, so that they can be used later when the function is
# invoked and the defining scope is no longer available.

def make_averager():
    count = 0
    total = 0
    # closure........
    def averager(new_value):
        nonlocal count, total #from py3 make excplicitly non local variable
        count += 1 # free variable
        total += new_value
        return total / count
    return averager

avg = make_averager()
print(avg(10)) #10.0
print(avg(11)) #10.5



# Remember that this code:
@clock
def factorial(n):
    return 1 if n < 2 else n*factorial(n-1)
#Actually does this:
def factorial(n):
    return 1 if n < 2 else n*factorial(n-1)
factorial = clock(factorial)


# stacked decorators
@d1
@d2
def f():
    print('f')

#is the same as
def f():
    print('f')

f = d1(d2(f))

# Parameters in decorators
registry = set()

def register(active=True):
    def decorate(func):
        print('running register(active=%s)->decorate(%s)' % (active, func))
        if active:
            registry.add(func)
        else:
            registry.discard(func)
        return func
    return decorate

@register(active=False) # the decorator must be invoked as a function w the desired params
def f1():
    print('running f1()')

@register()
def f2():
    print('running f2()')

def f3():
    print('running f3()')


import time

DEFAULT_FMT = '[{elapsed:0.8f}s] {name}({args}) -> {result}'

def clock(fmt=DEFAULT_FMT): # clock is our parameterized decorator factory
    def decorate(func): # decorate is the actual decorator
        def clocked(*_args): # clocks wraps the decorated function
            t0 = time.time()
            _result = func(*_args) # _result is the actual result of the decorated fn
            elapsed = time.time() - t0
            name = func.__name__
            args = ', '.join(repr(arg) for arg in _args) result = repr(_result) print(fmt.format(**locals()))
            return _result
        return clocked
    return decorate

if __name__ == '__main__':
    @clock()
    def snooze(seconds):
    time.sleep(seconds) for i in range(3):
            snooze(.123)
