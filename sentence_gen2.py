# A more lazy approach
#

import re
import reprlib

RE_WORD = re.compile('\w+')

class Sentence:
    def __init__(self, text):
        self.text = text # no need to have the words list

    def __repr__(self):
        return 'Sentence(%s)' % reprlib.repr(self.text)

    def __iter__(self):
        for match in RE_WORD.finditer(self.text): # finditer builds an iterator over the matches of RE_WORD on self.text yielding Match?Object instances
            yield match.group() # extracts the actual matched text from the MatchObject instance
        return # not needed

if __name__=='__main__':
    s = Sentence('my abc is made with fantastic books and some other stuff that li')
    it = iter(s)
    print(next(it))
