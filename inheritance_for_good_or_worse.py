

class DoppelDict(dict):
    def __setitem__(self, key, value):
        super().__setitem__(key, [value] * 2) # duplicates values when storing

dd = DoppelDict(one=1) # using the __init__ method here
dd # {'one':1}
dd['two'] = 2 # This operator calls __setitem__
dd # {'one': 1, 'two': [2, 2]}
dd.update(three=3) # This does not use __setitem__
dd # {'three': 3, 'one': 1, 'two': [2, 2]}

class AnswerDict(dict):
    def __getitem__(self, key):
        return 42 # __getitem__ always returns 42, no matter what the key.

ad = AnswerDict(a='foo') # ad is AnswerDict loaded with key-value pair ('a', 'foo')
ad['a'] # 42
d ={}
d.update(ad) # d is an instance of plain dict which is updated with ad
d['a'] # the update ignored our __getitem__ !!!!!!!!!
# 'foo'
d # {'a':'foo'}


# If you subclass UserDict the previous issue gets fixed

import collections

class DoppelDict2(collections.UserDict):
    def __setitem__(self, key, value):
        super().__setitem__(key, [value] * 2 )

dd = DoppelDict2(one=1)
dd # {'one': [1, 1]}
dd['two'] = 2
dd # {'two': [2, 2], 'one': [1, 1]}
dd.update(three=3)
dd # {'two': [2, 2], 'three': [3, 3], 'one': [1, 1]}

class AnswerDict2(collections.UserDict):
    def __getitem__(self, key):
        return 42

ad = AnswerDict2(a='foo')
ad['a'] # 42
d = {}
d.update(ad)
d['a'] # 42
d # {'a':42}


# A good example of multiinheritance is study the Django project in:
# https://github.com/django/django/blob/master/django/views/generic/base.py
