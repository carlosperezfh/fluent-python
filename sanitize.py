import unicodedata
import string

def shave_marks(txt):
    """Remove all diacritic marks"""
    norm_txt = unicodedata.normalize('NFD', txt) # Decompose all chars into base character and combining marks.
    shaved = ''.join(c for c in norm_txt if not unicodedata.combining(c)) # Filter out all combining marks

    return unicodedata.normalize('NFC', shaved) # Recompose all characters

order = '“Herr Voß: • 1⁄2 cup of ŒtkerTM caffè latte • bowl of açaí.”'
print(shave_marks(order))
#'“Herr Voß: • 1⁄2 cup of ŒtkerTM caffe latte • bowl of acai.”'
# Only the letters “è”, “ç” and “í” were replaced.

def shave_marks_latin(txt):
    """Remove all diacritic marks from Latin base characters"""
    norm_txt = unicodedata.normalize('NFD', txt)
    latin_base = False
    keepers = []
    for c in norm_txt:
        if unicodedata.combining(c) and latin_base: continue # ignore diacritic on Latin base char
        keepers.append(c)
        # if it isn't combining char, it's a new base char
        if not unicodedata.combining(c):
            latin_base = c in string.ascii_letters
    shaved = ''.join(keepers)
    return unicodedata.normalize('NFC', shaved)


single_map = str.maketrans(
    """‚ƒ„†ˆ‹‘’“”•–— ̃›""",
    """'f"*^<''""---~>"""
    ) # Building a map table for char to char replacement
multi_map = str.maketrans({ # Mapping table for char to string replacement
    '€': '<euro>',
    '...': '...',
    'Œ': 'OE',
    'TM': '(TM)',
    'œ': 'oe',
    '‰': '<per mille>',
    '‡': '**',
})
multi_map.update(single_map) # Merge mapping tables

def dewinize(txt):
    """Replace Win1252 symbols with ASCII chars or sequences"""
    return txt.translate(multi_map) # dewinize does not affect ASCII or latin1 text

def asciize(txt):
    no_marks = shave_marks_latin(dewinize(txt)) # Apply dewinize and remove diacritical marks.
    no_marks = no_marks.replace('ß', 'ss') # Replace the Eszett with “ss”
    return unicodedata.normalize('NFKC', no_marks) # Apply NFKC normalization to compose characters with their compatibility code points.

order = '“Herr Voß: • 1⁄2 cup of ŒtkerTM caffè latte • bowl of açaí.”'
print(dewinize(order)) #'"Herr Voß: - 1⁄2 cup of OEtker(TM) caffè latte - bowl of açaí."'
print(asciize(order)) # '"Herr Voss: - 1⁄2 cup of OEtker(TM) caffe latte - bowl of acai."'

# dewinize replaces curly quotes, bullets, and TM (trade mark symbol).
# asciize applies dewinize, drops diacritics and replaces the 'ß'.
