# Chp5 First class functions

def factorial(n):
    '''returns n!'''
    return 1 if n < 2 else n * factorial(n-1)
# >>> factorial(42) 1405006117752879898543142606244511569936384000000000 >>> factorial.__doc__
# 'returns n!'
# >>> type(factorial)
# <class 'function'> # This demostrates that a function is an instance of a class

fact = factorial
fact # <function factorial at 0x...
fact(5) #120
map(factorial, range(11)) # <map object at 0x..l
list(map(fact, range(11))) # [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800]

# A fn that takes a fn as an argument or returns a fn it's called higher-order function
# one example is map, sorted, etc...
fruits = ['strawberry', 'fig', 'apple', 'cherry', 'raspberry', 'banana']
sorted(fruits, key=len) #['fig', 'apple', 'cherry', 'banana', 'raspberry', 'strawberry']

def reverse(word):
    return word[::-1]

sorted(fruits, key=reverse)

# Lists of factorials producec with map and filter compared to altenatives coded as list comprehensions
list(map(fact, range(6))) # [1, 1, 2, 6, 24, 120]
[fact(n) for n in range(6)] # using a list comprehension; [1, 1, 2, 6, 24, 120]

list(map(factorial, filter(lambda n: n % 2, range(6)))) # [1, 6, 120]
[factorial(n) for n in range(6) if n % 2] # equivalent of map in list comprehension; [1, 6, 120]

# In py3 map and filter return generators. A form of iterator.
from functools import reduce
from operator import add

reduce(add, range(100)) # 4950
sum(range(100)) # 4950

# The idea of sum and reduce is apply some operation to successive items in a sequence, accumulating previous results,
# thus reducing a sequence of values to a single value

# Others are all(iterable); which returns True if every element of the iterable is truthy
# any(iterable); return True if any element of the iterable is truthy



# Anonymous functions
# The lambda keyword create an anonymous fn withing a Python expression
# The syntax limits the body of lambda fn to be pure expressions (cannot make assignments or use any other Python statement like while, try, etc)
# The best use of anonuymous fn is in the context of an argument list.

fruits = ['strawberry', 'fig', 'apple', 'cherry', 'raspberry', 'banana']
sorted(fruits, key=lambda word: word[::-1]) # ['banana', 'apple', 'fig', 'raspberry', 'strawberry', 'cherry']

# Seven flavors of callable objects

# The call operator () maybe be applied for other objects.
# To determine wheter an object is callable, use the callable() built-in fn.

"""
The Python Data Model documentation lists seven callable types:
    User-defined functions.
        Created with def statements or lambda expressions.

    Built-in functions
        a function implemented in C (for Cpython) like len or time.strftime

    Built-in methods
        methods implemented in C, like dict.get

    Methods
        functions defined in the body of a class

    Classes
        when invoked, a class runs its __new__ method to create an instance, then __init__ to initialize it, and finally the instance is retuned to the caller.
        Since there is no new in Python, calling class is like calling a fn

    Class instances
        if a class defines __call__ method, then its instances may be invoked as functions.
            Check user defined callable types

    Generator functions
        Are unlike other callables in many respects. See generators chapter.



"""

# The safest way to determine whether an obj is callable is:
[callable(obj) for obj in (abs, str, 13)] # True, True, False

# User defined callable types
# check bingocall.py


# Function introspection
dir(factorial) # fn have many attributes beyond __doc__
#['__annotations__', '__call__', '__class__', '__closure__', '__code__', '__defaults__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__get__', '__getattribute__', '__globals__', '__gt__', '__hash__', '__init__', '__kwdefaults__', '__le__', '__lt__', '__module__', '__name__', '__ne__', '__new__', '__qualname__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__']

# A function uses  __dict__ to store user attributes assigned to it.

def upper_case_name(obj): # Django admin uses this approach
    return ("%s %s" % (obj.first_name, obj.last_name)).upper()
upper_case_name.short_description = 'Customer name'


class C: pass
obj = C()
def func(): pass
sorted(set(dir(func)) - set(dir(obj))) #['__annotations__', '__call__', '__closure__', '__code__', '__defaults__', '__get__', '__globals__', '__kwdefaults__', '__name__', '__qualname__']


def tag(name, *content, cls=None, **attrs):
    """Generate one or more HTML tags"""
    if cls is not None:
        attrs['class'] = cls
    if attrs:
        attr_str = ''.join(' %s="%s"' % (attr, value)
            for attr, value in sorted(attrs.items()))
    else:
        attr_str = ''
    if content:
        return '\n'.join('<%s%s>%s</%s>' %
        (name, attr_str, c, name) for c in content)
    else:
        return '<%s%s />' % (name, attr_str)

print(tag('body'))
print(tag('tag', 'body', 'head', atts=23))
print(tag('p', 'hello', 'world', cls='sidebar'))
print(tag(content='testing', name="img"))

my_tag = {'name': 'img', 'title': 'Sunset Boulevard',
 'src': 'sunset.jpg', 'cls': 'framed'}
print(tag(**my_tag)) # Prefixing a dict with ** passes all its items as a separate arguments which are boiunded to the named params 
# <img class="framed" src="sunset.jpg" title="Sunset Boulevard" />
