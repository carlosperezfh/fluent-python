import collections

Card = collections.namedtuple('Card', ['rank', 'suit'])

class FrenchDeck:
    ranks = [str(n) for n in range(2,11)] + list('JQKA')
    suits = 'spades diamonds clubs hears'.split()

    def __init__(self):
        self._cards = [Card(rank, suit) for suit in self.suits for rank in self.ranks]

    def __len__(self):
        return len(self._cards)

    def __getitem__(self, position):
        return self._cards[position]


#
# >>> beer_card = Card('7', 'diamonds')
# >>> beer_card
Card(rank='7', suit='diamonds')

deck = FrenchDeck()
len(deck)
# 52

# deck[n] is what the __getitem__ provides
# >>> deck[0]
Card(rank='2', suit='spades')
# >>> deck[-1]
Card(rank='A', suit='hearts')

# __getitem__ also help us to use slices and make it iterable and using for loops
# If a collection has not __contains__ methods, the in operator does a sequential scan.

Card('Q', 'hearts') in deck # True

suit_values = dict(spades=3, hearts=2, diamonds=1, clubs=0)
def spades_high(card):
    rank_value = FrenchDeck.ranks.index(card.rank)
    return rank_value * len(suit_values) + suit_values[card.suit]

for card in sorted(deck, key=spades_high): # doctest: +ELLIPSIS ...
    print(card)

# Card(rank='2', suit='clubs')
# Card(rank='2', suit='diamonds')
# Card(rank='2', suit='hearts')
# ...
# (46 cards ommitted)
# Card(rank='A', suit='diamonds')
# Card(rank='A', suit='hearts')
# Card(rank='A', suit='spades')


# By implementing those special methods we are able to use our new class
# with standard python operations

# implementing a simple Vector class

from math import hypot
class Vector:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Vector(%r, %r)' % (self.x, self.y)

    def __abs__(self):
        return hypot(self.x, self.y)

    def __bool__(self):
        return bool(abs(self))

    def __add__(self, other):
        x = self.x + other.x
        y = self.y + other.y
        return Vector(x, y)

    def __mul__(self, scalar):
        return Vector(self.x * scalar, self.y * scalar)


# __repr__ is the string representation of the object for inspection.
# The book shows an extensive list of special methods and its usage,
# I've seen most of these in FH to perform arythmetical operations like in the Receipt class
