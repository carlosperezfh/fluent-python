
from array import array
import reprlib
import math
import numbers
import functools
import operator
import itertools

class Vector:
    typecode = 'd'
    def __init__(self, components):
        self._components = array(self.typecode, components)

    def __iter__(self):
        return iter(self._components)

    def __repr__(self):
        components = reprlib.repr(self._components)
        components = components[components.find('['):-1]
        return 'Vector({})'.format(components)

    def __str__(self):
        return str(tuple(self))

    def __bytes__(self):
        return (bytes([ord(self.typecode)]) + bytes(self._components))

    def __eq__(self, other):
        if isinstance(other, Vector):
            return (len(self) == len(other) and
                all(a == b for a, b in zip(self, other)))
        else:
            raise NotImplemented

    def __hash__(self):
        hashes = (hash(x) for x in self)
        return functools.reduce(operator.xor, hashes, 0)

    def __abs__(self):
        return math.sqrt(sum(x * x for x in self))

    def __bool__(self):
        return bool(abs(self))

    def __len__(self):
        return len(self._components)

    def __getitem__(self, index):
        cls = type(self)
        if isinstance(index, slice):
            return cls(self._components[index])
        elif isinstance(index, numbers.Integral):
            return self._components[index]
        else:
            msg = '{.__name__} indices must be integers'
            raise TypeError(msg.format(cls))

    shortcut_names = 'xyzt'

    def __getattr__(self, name):
        cls = type(self)
        if len(name) == 1:
            pos = cls.shortcut_names.find(name)
        if 0 <= pos < len(self._components):
            return self._components[pos]
        msg = '{.__name__!r} object has no attribute {!r}'
        raise AttributeError(msg.format(cls, name))

    def __add__(self, other):
        try:
            pairs = itertools.zip_longest(self, other, fillvalue=0.0) # generator that produces tuples of (a,b)
            return Vector(a + b for a, b in pairs) # A new Vector is built from the generator producing the sum for item in pairs
        except TypeError:
            raise NotImplemented # to avoid typeERror standard messages

    def __radd__(self, other):
        """reflected or reversed version of add
        to make mixed type additions
        radd just delegates to __add__
        """
        return self + other

    def __mul__(self, scalar):
        if isinstance(scalar, numbers.Real):
            return Vector(n * scalar for n in self)
        else:
            raise NotImplemented

    def __rmul__(self, scalar):
        return self * scalar


    def angle(self, n):
        r = math.sqrt(sum(x * x for x in self[n:]))
        a = math.atan2(r, self[n-1])
        if (n == len(self) - 1) and (self[-1] < 0):
            return math.pi * 2 - a
        else:
            return a

    def angles(self):
        return (self.angle(n) for n in range(1, len(self)))

    # def __format__(self, fmt_spec=''):
    #     if fmt_spec.endswith('h'): # hyperspherical coordinates
    #             fmt_spec = fmt_spec[:-1]
    #             coords = itertools.chain([abs(self)],
    #             outer_fmt = '<{}>'
    #     else:
    #         coords = self
    #     self.angles())
    #     outer_fmt = '({})'
    #     components = (format(c, fmt_spec) for c in coords) return outer_fmt.format(', '.join(components))


v1 = Vector([3, 4, 5])
print(v1 + (10,20, 30)) # This works withouth __radd because __add uses zip_longest which consumes any iterable
# however if we swap the operands the operation fails. TypeError: can only concatenate tuple (not "Vector") to tuple
print((10, 20, 30) + v1) # after implementing __radd it works.

# if a has __add__, call a.__add__(b)
# if a doesn't have __add__ or calling it returns NotImplemented, check if b has
# __radd__, then call b.__radd__(a) and return result unles ?NotImplemented
# Else raise TypeERror

print(10.0*Vector([2, 4, 5])) # (20.0, 40.0, 50.0)

from fractions import Fraction
print(v1 * Fraction(1, 3)) # (1.0, 1.3333333333333333, 1.6666666666666665)

va = Vector([1.0, 2.0, 3.0])
vb = Vector(range(1, 4))

va == vb # True
t3=(1,2,3)
va == t3 # Our NotImplemented raise in __eq__
