# The dictionary

# Python dicts are highly optimized. Hash tables are the engines behind Python's high performance dicts.
# An obj is hashable if it has a hash value which never changes during its lifetime (__hash__ method) an can be compared
# with other objects (needs __eq__)/
# A frozen set is always hashable, a tuple is hashable only if all its items are hashable.

tt = (1, 2, (30, 40))
hash(tt) # some big number like 8027212646858338501
# tl = (1, 2, [30, 40])
#Traceback (most recent call last):
#File "<stdin>", line 1, in <module> TypeError: unhashable type: 'list'
tf = (1, 2 , frozenset([30, 40]))
hash(tf) #       -4118419923444501110

# Building dictionaries in the following ways:
a = dict(one=1, two=2, three=3)
b = {'one': 1, 'two': 2, 'three': 3}
c = dict(zip(['one', 'two', 'three'], [1, 2, 3]))
d = dict([('two', 2), ('one', 1), ('three', 3)])
e = dict({'three': 3, 'one': 1, 'two': 2})
a == b == c == d == e # True

# dict comprehensions
# We can also build a dict using any iterable
DIAL_CODES =  [
 (86, 'China'),
 (91, 'India'),
 (1, 'United States'),
 (62, 'Indonesia'),
 (55, 'Brazil'),
 (92, 'Pakistan'),
 (880, 'Bangladesh'),
 (234, 'Nigeria'),
 (7, 'Russia'),
 (81, 'Japan'),
]

country_code = {country:code for code, country in DIAL_CODES} # pairs are reversed: country is the key, and code is the value

filtered_country_code = {code: country.upper() for country, code in country_code.items() if code < 66} # Reversing the pairs again, values upper-cased and items filtered by code < 66
# country_code.items() returns a list of (key,value) pairs


# Using setdefault
#example withouth setdefault
# >>> ocurrences = index.get(word, [])
# >>> ocurrences.append(location)
# >>> index[word] = occurrences

# those 3 lines can be replaced by:
# >>> index.setdefault(word, []).append(location)


# Using defaultdict
import collections
index = collections.defaultdict(list) # Create a defaultdict with the list constructor as default_factory
index['something-new'].append('New Value') # if `something-new` is not initially in the index, the default_factory is called to produce the missing value, which is an empty list and the operation always suceeds
# If not defaultdict is provided, the usual KeyError is raised for missing keys


ct = collections.Counter('abracadabra')
ct # Counter('a': 5....)
ct.update('aaaaaaaaaaaazzzzzzz')
ct # Counter('a':17, 'z':7, 'b':2......)
ct.most_common(2) # [('a', 17), ('z', 7)]

# Python3 only
class StrKeyDict(collections.UserDict):
    def __missing__(self, key):
        if isinstance(key, str):
            raise KeyError(key)
        return self[str(key)]

    def __contains__(self, key):
        return str(key) in self.data

    def __setitem__(self, key, item):
        self.data[str(key)] = item

# Sets in Python
# A set is a colletion of unique objects. A basic use case ir removing duplication:
l = ['spam', 'spam', 'eggs', 'spam']
set(l) # {'eggs', 'spam'}

# In addition to uniqueness, sets implements the essential set operations as infix operators,
# like a | b for the union, a & b for the interection and a - b for the difference

s = {1}
type(s) #<class 'set'>
s # {1}

# set comprehentions
from unicodedata import name
{chr(i) for i in range(32, 256) if 'SIGN' in name(chr(i), '')} # {'§', '=', '¢', '#', '¤', ......

import pdb; pdb.set_trace()
