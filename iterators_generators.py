"""
Every generator is an iterator: generators fully implement the iter‐ ator interface.
But an iterator — as defined in the GoF book — re‐ trieves items from a collection,
while a generator can produce items “out of thin air”.
That’s why the Fibonacci sequence generator is a common example:
an infinite series of numbers cannot be stored in a collection. However,
be aware that the Python community treats iterator and generator as synonyms most of the time.
"""

# start in sentence.py
# Why Sentence is iterable?
"""
Whenever the interpreter needs to iterate over an object x, it automatically calls iter(x).
The iter buit-in function:
 - Checks wheter the obj implements __iter__, and calls that to obtain the iterator.
 - If __iter__ is not implemented, but __getitem__ is implemented, Python creates an iterator that attempts to fetch items in order, starting from idx 0
 - If that fails, Python raises TypeError "'C' object is not itera ble", where C is the class of the target object

All python iterable objects implement __getitem__.
In fact standard sequences also implement __iter__, and yours should too, because the special handling of getitem

an object is considered iterable not only when it implements the special method __iter__,
but also when it implements __getitem__, as long as __getitem__ accepts int keys starting from 0.
"""


"""
Iterables vs Iterators

Iterable
    Any object which the iter built-in fn can obtain an iterator....
    Objects implementing an __iter__ method returning an iterator are iterable......
    Sequences are always iterable; so as are objects implementing a __getitem__ method which takex 0-based idx

iterator
    Any object that implements the __next__ no-argument method which returns the next item in a series or raises
    StopIteration when there are no more items.
    Python iterators also implement the __iter__ method so they are iterable as well.


It's important that there are iterables and iterators
iterable is the object like str='ABC'
and iterator is the object that implements __iter__ and __next__


"""
s = 'ABC'
for char in s:
    print(char)

# A
# B
# C

it = iter(s) # it is the iterator here
while True:
    try:
        print(next(it)) # Repeatedly call next on the iterator to obtain the next item
    except StopIteration:
        del it
        break

from sentence import Sentence
s3 = Sentence('Pig and Pepper')
it = iter(s3)
print(it) # <iterator object at 0x...>
print(next(it)) # Pig
print(next(it)) # and
print(next(it)) # Pepper

#print(next(it))
# Traceback (most recent call last):
#   File "iterators_generators.py", line 66, in <module>
#     print(next(it))
# StopIteration

"""
How a generator fn works
(see sentence_gen.py)
Any python object that has the yield keyword in its body its a generator function:
a function which, when called, returns a generator object.


"""

def gen_123():
    yield 1
    yield 2
    yield 3

gen_123 # A function object. <function gen_123 at 0x...> #
gen_123() # But when invoked, we see a generator. <generator object gen_123 at 0x...> #
for i in gen_123(): # Generators are iterators that produce the values of the expressions passed to yield
    print(i)
# 1
# 2
# 3

g = gen_123() # for closer inspection
next(g) # 1, since g is an iterator, calling next(g) fetches the next item produced by yield
next(g) # 2
next(g) # 3

# next(g) When its completed, the generator obj raises StopIteration
#Traceback (most recent call last):
#...
#    StopIteration

# A generator expression can be understood as a lazy version of a list comprehension: it does not eagerly build a list,
# but returns a generator that will lazily produce the items on demand. In other words if a list comp is a factory of lists
# a generator expression is a factory of generators

def gen_AB():
    print('start')
    yield 'A'
    print('continue')
    yield 'B'
    print('end')

res1 = [x*3 for x in gen_AB()] #The list comprehension eagerly iterates over the items yielded
#by the generator object produced by calling gen_AB(): 'A' and 'B'. Note the output in the next lines: start, continue, end.

#start
#continue
#end.

fori in res1: # This for loop is iterating over the the res1 list produced by the list comprehension.
    print('-->', i)

# AAA
# BBB

res2 = (x*3 for x in gen_AB()) # The generator expression returns res2. The call to gen_AB() is made,
#$ but that call returns a generator which is not consumed here

res2 # <generator object <genexpr> at 0x10063c240>, res2 is a generator object.

for i in res2: # Only when the for loop iterates over res2, the body of gen_AB actually executes.
    print('-->', i)

#start
#--> AAA
#continue
#--> BBB
#end.


# See. sentence_genexp.py


class ArithmeticProgression:
    def __init__(self, begin, step, end=None):
        self.begin = begin
        self.step = step
        self.end = end # None -> "infinite" series

    def __iter__(self):
        result = type(self.begin + self.step)(self.begin) forever = self.end is None
        index = 0
        while forever or result < self.end:
            yield result
            index += 1
            result = self.begin + self.step * index

"""
 >>> ap = ArithmeticProgression(0, 1, 3)
>>> list(ap)
[0, 1, 2]
>>> ap = ArithmeticProgression(1, .5, 3)
>>> list(ap)
[1.0, 1.5, 2.0, 2.5]
>>> ap = ArithmeticProgression(0, 1/3, 1)
>>> list(ap)
[0.0, 0.3333333333333333, 0.6666666666666666]
>>> from fractions import Fraction
>>> ap = ArithmeticProgression(0, Fraction(1, 3), 1) >>> list(ap)
[Fraction(0, 1), Fraction(1, 3), Fraction(2, 3)] >>> from decimal import Decimal
>>> ap = ArithmeticProgression(0, Decimal('.1'), .3) >>> list(ap)
[Decimal('0.0'), Decimal('0.1'), Decimal('0.2')]
"""
