class A:
    def ping(self):
        print('ping', self)

class B(A):
    def pong(self):
        print('pong', self)

class C(A):
    def pong(self):
        print('PONG:', self)

class D(B, C):
    def ping(self):
        super().ping()
        print('post-ping', self)

    def pingpong(self):
        self.ping()
        super().ping()
        self.pong()
        super().pong()
        C.pong(self)


d = D()
d.ping()
d.pong()
C.pong(d)
print('-------')
d.pingpong()
print('-------')
d.pingpong()
# Python follows a specific order when traversing the inheritance graph. That order is called MRO: Method Resolution Order
print(D.__mro__) # (<class '__main__.D'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.A'>, <class 'object'>)

def print_mro(cls):
    print(', '.join(c.__name__ for c in cls.__mro__))

print_mro(bool)
