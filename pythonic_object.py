# Chapter 9 pythonic objects

# staticmethod vs classmethod

class Demo:
    @classmethod
    def klassmeth(*args):
        return args # just returns all positional args

    @staticmethod
    def statmeth(*args):
        return args # does the same as klassmeth

Demo.klassmeth() # No matter how you invoke it, Demo.klassmeth receives the Demo class as the first argument
# (<class '__main__.Demo'>,)

Demo.klassmeth('spam')
# (<class '__main__.Demo'>, 'spam')

Demo.statmeth() # Demo.statmeth behaves just like a plain old function.
# ()

Demo.statmeth('spam')
# ('spam',)

# Use staticmethod if you want to define a fn that does not interact with the class



# Formatted displays

format(42, 'b')
# '101010'
from datetime import datetime

now = datetime.now()
format(now, '%H:%M:%S')
# '18:49:05'

"It's now {:%I:%M %p}".format(now)
#"It's now 06:49 PM"

# In a class we need to implement our own __format__ method

def __format__(self, fmt_spec=''):
    components = (format(c, fmt_spec) for c in self)
    return '({}, {})'.format(*components)


# Using zip
# zip helps up to iterate in parallel over two or more iterables by returning
#tuples what you can unpack into variables, one for each iten in the paralell inputs

zip(range(3), 'ABC') # <zip object at 0x10063ae48>
list(zip(range(3), 'ABC')) # [(0, 'A'), (1, 'B'), (2, 'C')]
list(zip(range(3), 'ABC', [0.0, 1.1, 2.2, 3.3])) # [(0, 'A', 0.0), (1, 'B', 1.1), (2, 'C', 2.2)]

from itertools import zip_longest # zip_longest behaves differently, it uses an optional fillvalue (None by def) to complete missing values so it can generate tuples until the last iterable is exhausted
list(zip_longest(range(3), 'ABC', [0.0, 1.1, 2.2, 3.3], fillvalue=-1))
#[(0, 'A', 0.0), (1, 'B', 1.1), (2, 'C', 2.2), (-1, -1, 3.3)]
