

x = 'ABC'
dummy = [ord(x) for x in x]
x
dummy

#python2 output (bad bad py2)
#C
#[65, 66, 67]

#python3 output
#ABC # Thevalue of x is preserved (good py3)
#[65, 66, 67]


# Listcomps can generate lists from the carterian product of two or more iterables.
# The resulting lengs is equals the lengts of the input iterable multiplied
# Example

colors = ['black', 'white']
sizes = ['S', 'M', 'L']
tshirts = [(color, size) for color in colors for size in sizes] # This generates a list of tuples arranged by color, then size.
# >>> tshirts
# [('black', 'S'), ('black', 'M'), ('black', 'L'), ('white', 'S'),
# ('white', 'M'), ('white', 'L')]

#To get items arranged by size, then color, just rearrange the for clauses;
#adding a line break to the listcomp makes it easy to see how the result will be ordered.
tshirts = [(color, size) for size in sizes for color in colors]
# >>> tshirts
# [('black', 'S'), ('white', 'S'), ('black', 'M'), ('white', 'M'),
#  ('black', 'L'), ('white', 'L')]


# Generator expressions

# Initializes tuples, arrays and other types of sequences, genexp saves memory because
# it yields items one by one using the iterator protocol.
# Same syntax as listcomps but are enclosed with parentheses rather than brackets

symbols = '$¢£¥€¤'
tuple(ord(symbol) for symbol in symbols)

# Using generator expressions to get the cartessian product of colors and sizes,
# using this approach, if the input arrays contains thousands of elements
# is going to be more inefficient since its producing one item at a time

for tshirt in ('%s %s' % (c, s) for c in colors for s in sizes):
    print(tshirt)


# Tuples as records
lax_coordinates = (33.9425, -118.408056)
city, year, pop, chg, area = ('Tokyo', 2003, 32450, 0.66, 8014)
traveler_ids = [('USA', '31195855'), ('BRA', 'CE342567'),('ESP', 'XDA205856')]

for passport in sorted(traveler_ids):
    print('%s/%s' % passport)

# >>>t=(20,8)
# >>> divmod(*t) # Another example of tuple unpacking is prefix with * an argument

# Using * to grab excess items
# Defining fn parameters with *args to grab arbitrary excess arguments is a classic python feature

# But in Py3 this was extended to apply to parallel assignment as well:
a, b, *rest = range(5)
a, b, rest
(0, 1, [2, 3, 4])

# Named tuples
# The collections.namedtuple fn is a factory that produces subclasses of tuple enhaced
# with field names and a class name

from collections import namedtuple
City = namedtuple('City', 'name country population coordinates')
tokyo = City('Tokyo', 'JP', 36.933, (35.689722, 139.691667))
print(tokyo)
# City(name='Tokyo', country='JP', population=36.933, coordinates=(35.689722, 139.691667))
print(tokyo.population)
#36.933
print(tokyo.coordinates)
#(35.689722, 139.691667)
print(tokyo[1]) # you can access the fields by name or position
# JP


# Slices

# To evaluate the expression seq[start:stop:step]

l=[10,20,30,40,50,60]
l[:2] # split at 2
# [10, 20]
l[2:]
# [30, 40, 50, 60]
l[:3] # split at 3 [10, 20, 30]
#  Slicing | 33
# >>> l[3:] [40, 50, 60]

# >>> s = 'bicycle'
# >>> s[::3]
# 'bye'
# >>> s[::-1] 'elcycib'
# >>> s[::-2] 'eccb'

#Building lists of listc
board = [['_'] * 3 for i in range(3)]
board
# [['_', '_', '_'], ['_', '_', '_'], ['_', '_', '_']]

# Arrays
# More efficient than lists when using numbers
from array import array
from random import random
floats = array('d', (random() for i in range(10**7))) # CReate an array of doubles 'd' frmo any iterable object, in this case a generator
floats[-1]
# fp  = open('floats.bin', 'wb')
# floats.tofile(fp)
# fp.close()


# Changing the value of an array item by poking one of its bytes.
numbers = array('h', [-2, -1, 0, 1, 2]) # array of 5 short signed integers (typecode 'h')
memv = memoryview(numbers) #

len(memv) # 5
memv[0] # -2
memv_oct = memv.cast('B') # cast elements of memv to typecode 'B' (unsigned char)
memv_oct.tolist() # [254, 255, 255, 255, 0, 0, 1, 0, 2, 0]

# Collections deque

# Designed for fast inserting and removing from both ends (left and right).

from collections import deque
dq = deque(range(10), maxlen=10) # optional maxlen sets the max number of items allowed in this instances of deque
dq # deque([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], maxlen=10)
dq.rotate(3) # Rotate when n > 0 takes items from the right end and prepends to the left
dq # deque([7, 8, 9, 0, 1, 2, 3, 4, 5, 6], maxlen=10)
dq.rotate(-4)  # When n<0 items are taken from left and appended to the right
dq # deque([1, 2, 3, 4, 5, 6, 7, 8, 9, 0], maxlen=10)
dq.appendleft(-1) # Appending to a deque that is full len(d) == d.maxlen discards items from the other end; note in the next line that 0 is dropped
dq # deque([-1, 1, 2, 3, 4, 5, 6, 7, 8, 9], maxlen=10)
dq.extend([11, 22, 33]) # Adding three items to the right pushes out the left most -1, 1, and 2;
dq # deque([3, 4, 5, 6, 7, 8, 9, 11, 22, 33], maxlen=10)
dq.extendleft([10,20,30,40]) # extendleft works by appending each successive item of the iter arg to the left of the deque, therefore the final position of items is reversed
dq # deque([40, 30, 20, 10, 3, 4, 5, 6, 7, 8], maxlen=10)
