registry = []
def register(func):
    print('running register(%s)' % func)
    registry.append(func)
    return func

@register
def f1():
    print("Running f1()")

@register
def f2():
    print("Running f2()")

def f3():
    print("Running f3()")

def main():
    print('running main()')
    print('registry ->', registry)
    f1()
    f2()
    f3()

if __name__=='__main__':
    main()

# running register(<function f1 at 0x104446dd0>)
# running register(<function f2 at 0x104446ef0>)
# running main()
# registry -> [<function f1 at 0x104446dd0>, <function f2 at 0x104446ef0>]
# Running f1()
# Running f2()
# Running f3()


# IF you import it from another module, you will see:

#>>> import registration
#running register(<function f1 at 0x10063b1e0>)
#running register(<function f2 at 0x10063b268>)
