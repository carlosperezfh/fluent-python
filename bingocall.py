import random

class BingoCage:
    def __init__(self, items):
        """Accepts any iterable;

        """
        self._items = list(items) #building a local copy prevents unexpected side effects on any list passed as an argument
        random.shuffle(self._items) # shuffle is guaranteed to work as n argument

    def pick(self):
        """The main method
        """
        try:
            return self._items.pop()
        except IndexError:
            raise LookupError('pick from empty BingoCage') # Raise an exception with custom message if the list is empty

    def __call__(self):
        return self.pick() # shortcut to bingo.pick(): bingo()


bingo = BingoCage(range(3))
bingo.pick() # 1

bingo() # 0

callable(bingo) # True


# Another example of using __call__ to make function-like objects is in Decorators and Closures.
