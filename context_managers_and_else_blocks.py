"""
Here are the rules for else blocks:
for
    The else block will run only if and when the for loop runs to completion;
    i.e. not if the for is aborted with a break.
while
    The else block will run only if and when the while loop exits because the condition became falsy;
    i.e. not when the while is aborted with a break.
try
    The else block will only run if no exception is raised in the try block.
    The official docs also state: “Exceptions in the else clause are not handled by the preceding except clauses.”
"""
for item in my_list:
    if item.flavor == 'banana':
        break
    else:
        raise ValueError('No banana flavor found!')

"""
try:
    dangerous_call()
    after_call() # there is not good reason to keep this
except OSError:
    log('OSError...')

The try block should only have the statements that may generate the expected exceptions.
This is much better:

try:
    dangerous_call()
except OSError:
    log('OSError...')
else:
    after_call()
"""


# Context managers and with blocks
"""
The with statement was designed to simplify the try/finally pattern which guarantees that some operation is performed after a block of code
even if the block is aborted because of an exception, a return or sys.exit() call. The code in the finally clause usually releases a critial resource or restores
some previous state that was temporarily changed.

The context manager protocol consists of the __enter__ and __exit__ methods.
At the start of the with, __enter__ is invoked on the context manager object.
The role of the finally clause is played by a call to __exit__ on the context manager object at the end of the with block.

with open('mirror.py') as fp:
    src = fp.read(60)

>>> fp
<_io.TextIOWrapper name='mirror.py' mode='r' encoding='UTF-8'>
>>> fp.closed, fp.encoding
(True, 'UTF-8')
>>> fp.read(60)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: I/O operation on closed file.


"""

from mirror import LookingGlass
with LookingGlass() as what: # The context mnger is an instance of LookingGlass; Python calls __enter__ on the context manager and the result is bound to what
    print('Alice, Kitty and Snowdrop') # print some string and the value of what
    print(what) #pordwonS dna yttiK ,ecilA

# pordwonS dna yttiK ,ecilA
# YKCOWREBBAJ
what # 'JABBERWOCKY'. wht with block is over
print('Back to normal.')
#Back to normal. Program output is no longer backwards.

# The contextlib utilities

# Using @contextmanager

"""The @contextmanager decorator reduces the boilerplate of creating a context manager:
instead of writing a whole class with __enter__/__exit__ methods, you just implement a generator
with a single yield that should produce whatever you want the __en ter__ method to return.

In a generator decorated with @contextmanager, yield is used to split the body of the function in two parts:
everything before the yield will be executed at the beginning of the while block when the interpreter calls __enter__;
the code after yield will run when __exit__ is called at the end of the block.
"""

# check mirror_gen.py: a context manager implemented with a generator.
from mirror_gen import looking_glass
with looking_glass() as what: # looking_glass instead of LookingGlass.
    print('Alice, Kitty and Snowdrop')
    print(what)

# pordwonS dna yttiK ,ecilA
# YKCOWREBBAJ
# >> what
# 'JABBERWOCKY'
