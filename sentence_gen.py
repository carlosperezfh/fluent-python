# A more idiomatic approach

import re
import reprlib

RE_WORD = re.compile('\w+')

class Sentence:
    def __init__(self, text):
        self.text = text
        self.words = RE_WORD.findall(text)

    def __repr__(self):
        return 'Sentence(%s)' % reprlib.repr(self.text)

    def __iter__(self):
        for word in self.words: # iterator over self.words
            yield word # yield the current word
        return # not needed

if __name__=='__main__':
    s = Sentence('my abc is made with fantastic books and some other stuff that li')
    it = iter(s)
    print(next(it))
