# Chapter 4. Text vs bytes
# This ch deals w unicode strings, bynary sequences and the encodings used to covert between them

# in Py3 str are Unicode characters just like the Unicode object in py2 and not the raw bytes you get from a py2 str

# Converting from code points to bytes is encoding; from bytes to code points is decoding.

s = 'café'
len(s) # 4, the str 'café' has 4 Unicode characters
b = s.encode('utf8') # Encode str to bytes using UTF-8 characters
b # b'caf\xc3\xa9; bytes literals start with a b prefix
len(b) # 5; b has five bytes ("é" is enoded as two bytes in UTF-8)
b.decode('utf8') # Decode bytes to str using UTF-8 enconding
b # 'café'

# We decode bytes to str to get human readable text
# We encode str to bytes for storage or transmission

for codec in ['latin_1', 'utf_8', 'utf_16']: # python bundles more than 100 codecs (encoder/decoder) for text to byte conversion and vice-versa
    print(codec, 'El Niño'.encode(codec), sep='\t')

octets = b'Montr\xe9al'
octets.decode('cp1252') # 'Montréal' ; Decoding w Windows 1252 works because it is a proper superset of latin1
octets.decode('iso8859_7') # 'Montrιal'; Intended for Greek, the byte is misinterpreted and no error is issued
#octets.decode('utf_8') # UnicodeDecodeError: 'utf-8' codec can't decode byte 0xe9 in position 5; utf_8 detects that octets is not a valid UTF8 and raises error
octets.decode('utf_8', errors='replace') # 'Montr�al' # Using replace error handling

# UTF-8 is the default source encoding for Py3, just as ACII was the default for Py2

# Best practice for handling text is the "Unicode sandwich"
# Bytes should be decoded to str as easly as possible on input.
# The "meat" of the sandwich is the business logic of your propgramm, where text handling
# is done exluively on str objs.
# Django itself takes care of encoding the response to bytes

# Always pass an explicit encoding= argument when opening text files,
# becase the default may change from one machine to the next, or from one day to the next.

# Check default_encodings.py and the output shows UTF-8 on Ubuntu and OSX since its the default on these systems
# In windows the output might be different (specially if your language is not english)



# Normalizing Unicode for saner comparisons
s1 = 'café'
s2 = 'cafe\u0301'
s1, s2 # ('café', 'café')
len(s1), len(s2) # (4, 5)
s1 == s2 # False

from unicodedata import normalize, name
# Normalization Form C (NFC) composes the code points to produce the shortest equivalent string,
# while NFD desponseses
len(normalize('NFC', s1)), len(normalize('NFC', s2)) # (4, 4)
len(normalize('NFD', s1)), len(normalize('NFD', s2)) # (5, 5)
normalize('NFC', s1) == normalize('NFC', s2) # True
normalize('NFD', s1) == normalize('NFD', s2) # True

ohm = '\u2126'
name(ohm) # 'OHM SIGN'
ohm_c = normalize('NFC', ohm)
name(ohm_c) #'GREEK CAPITAL LETTER OMEGA'
ohm == ohm_c # False

normalize('NFC', ohm) == normalize('NFC', ohm_c) # True


import unicodedata
import re

re_digit = re.compile(r'\d')
sample = '1\xbc\xb2\u0969\u136b\u216b\u2466\u2480\u3285'

for char in sample:
    print('U+%04x' % ord(char), # Code point in U+0000 format
        char.center(6), # Character centralized in a str of len 6
        're_dig' if re_digit.match(char) else '-', # Show re_dig if character matches the r'\d' regex
        'isdig' if char.isdigit() else '-', # Show isdigit if char.isdigit() is True
        'isnum' if char.isnumeric() else '-', # Shows isnum if char.isnumeric() is True
        format(unicodedata.numeric(char), '5.2f'), # Numberic value formated with width 5 and 2 decimal places
        unicodedata.name(char), # Unicode character name
        sep='\t')

# The output is:
# U+0031	  1   	re_dig	isdig	isnum	 1.00	DIGIT ONE
# U+00bc	  ¼   	-	-	isnum	 0.25	VULGAR FRACTION ONE QUARTER
# U+00b2	  ²   	-	isdig	isnum	 2.00	SUPERSCRIPT TWO
# U+0969	  ३   	re_dig	isdig	isnum	 3.00	DEVANAGARI DIGIT THREE
# U+136b	  ፫   	-	isdig	isnum	 3.00	ETHIOPIC DIGIT THREE
# U+216b	  Ⅻ   	-	-	isnum	12.00	ROMAN NUMERAL TWELVE
# U+2466	  ⑦   	-	isdig	isnum	 7.00	CIRCLED DIGIT SEVEN
# U+2480	  ⒀   	-	-	isnum	13.00	PARENTHESIZED NUMBER THIRTEEN
# U+3285	  ㊅   	-	-	isnum	 6.00	CIRCLED IDEOGRAPH SIX
