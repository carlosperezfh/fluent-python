class LookingGlass:
    def __enter__(self):
        """python invokes enter with no arguments besides self"""
        import sys
        self.original_write = sys.stdout.write # hold the original write for later use
        sys.stdout.write = self.reverse_write # Monkey-patch sys.s...write replacing it with our own method
        return 'JABBERWOCKY' # return the string just to have something in the variable what

    def reverse_write(self, text):
        """our replacement to sys...write. reverses the text argument and calls the original implementation"""
        self.original_write(text[::-1])

    def __exit__(self, exc_type, exc_value, traceback):
        """Python calls __exit__ with None, None, None if all went well"""
        import sys # It’s cheap to import modules again because Python caches them.
        sys.stdout.write = self.original_write
        if exc_type is ZeroDivisionError:
            print('Please do not divide by zero')
            return True
